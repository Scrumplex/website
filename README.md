Scrumplex Website
-----------------
Personal website of Sefa Eyeoglu. Hosted at [scrumplex.net](https://scrumplex.net).

# Deployment

## Docker

```sh
$ docker build -t website:latest .
$ docker run -p 80:80 website:latest
```

## Buildah + Podman

```sh
$ buildah bud -t website:latest .
$ podman run -p 80:80 website:latest
```

## Traditional

```sh
$ git clone https://git.sr.ht/~scrumplex/website
$ cd website
$ yarn
$ yarn run build

# host static content
$ cd dist
$ python -m http.server
```

# License

This project is licensed under the GNU General Public License v3. You can read the full license in
the [LICENSE](LICENSE) file.
