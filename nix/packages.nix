{
  inputs,
  self,
  ...
}: let
  version = builtins.substring 0 8 self.lastModifiedDate or "dirty";
in {
  imports = [inputs.flake-parts.flakeModules.easyOverlay];

  perSystem = {
    config,
    final,
    ...
  }: {
    packages = {
      default = config.packages.scrumplex-website;
      scrumplex-website = final.callPackage ./pkgs/scrumplex-website.nix {inherit version;};
    };
    overlayAttrs = {
      inherit (config.packages) scrumplex-website;
    };
  };
}
