{lib, ...}: {
  perSystem = {config, ...}: {
    checks = lib.mapAttrs' (n: lib.nameValuePair "package-${n}") config.packages;
  };
}
