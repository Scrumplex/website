{
  lib,
  buildNpmPackage,
  importNpmLock,
  pkg-config,
  python3,
  vips,
  version,
}: let
  src = lib.fileset.toSource {
    root = ../../.;
    fileset = lib.fileset.unions (map (fileName: ../../${fileName}) [
      "assets"
      "src"
      ".eslintrc.json"
      ".parcelrc"
      "package.json"
      "package-lock.json"
      "svgo.config.json"
    ]);
  };
in
  buildNpmPackage {
    pname = "scrumplex-website";
    inherit src version;

    nativeBuildInputs = [
      pkg-config
      # Workaround for https://github.com/NixOS/nixpkgs/issues/329108
      (python3.withPackages (ps: with ps; [distutils]))
    ];

    buildInputs = [
      vips
    ];

    npmDeps = importNpmLock {
      npmRoot = src;
    };

    npmConfigHook = importNpmLock.npmConfigHook;

    installPhase = ''
      runHook preInstall

      cp -Tr dist/ $out

      runHook postInstall
    '';

    meta = with lib; {
      description = "Personal website";
      homepage = "https://codeberg.org/Scrumplex/website";
      license = licenses.agpl3Only;
      maintainers = with maintainers; [Scrumplex];
      platforms = platforms.all;
    };
  }
