{...}: {
  perSystem = {
    config,
    pkgs,
    self',
    ...
  }: {
    devShells.default = pkgs.mkShellNoCC {
      shellHook = ''
        ${config.pre-commit.installationScript}
      '';

      packages = [self'.formatter pkgs.nodejs_latest];
    };
    formatter = pkgs.alejandra;
    pre-commit.settings = {
      excludes = ["flake.lock" "package-lock.json"];
      hooks = {
        alejandra.enable = true;
        nil.enable = true;
        prettier.enable = true;
      };
    };
  };
}
