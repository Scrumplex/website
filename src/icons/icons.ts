export type IconsId =
    | "00-baseline"
    | "codeberg"
    | "github_sponsors"
    | "github"
    | "gitlab"
    | "ko-fi"
    | "liberapay"
    | "mail"
    | "matrix"
    | "new_tab"
    | "paypal"
    | "phone"
    | "teamspeak"
    | "telegram"
    | "wire";

export type IconsKey =
    | "i00Baseline"
    | "Codeberg"
    | "GithubSponsors"
    | "Github"
    | "Gitlab"
    | "KoFi"
    | "Liberapay"
    | "Mail"
    | "Matrix"
    | "NewTab"
    | "Paypal"
    | "Phone"
    | "Teamspeak"
    | "Telegram"
    | "Wire";

export enum Icons {
    i00Baseline = "00-baseline",
    Codeberg = "codeberg",
    GithubSponsors = "github_sponsors",
    Github = "github",
    Gitlab = "gitlab",
    KoFi = "ko-fi",
    Liberapay = "liberapay",
    Mail = "mail",
    Matrix = "matrix",
    NewTab = "new_tab",
    Paypal = "paypal",
    Phone = "phone",
    Teamspeak = "teamspeak",
    Telegram = "telegram",
    Wire = "wire",
}

export const ICONS_CODEPOINTS: { [key in Icons]: string } = {
    [Icons.i00Baseline]: "61697",
    [Icons.Codeberg]: "61698",
    [Icons.GithubSponsors]: "61699",
    [Icons.Github]: "61700",
    [Icons.Gitlab]: "61701",
    [Icons.KoFi]: "61702",
    [Icons.Liberapay]: "61703",
    [Icons.Mail]: "61704",
    [Icons.Matrix]: "61705",
    [Icons.NewTab]: "61706",
    [Icons.Paypal]: "61707",
    [Icons.Phone]: "61708",
    [Icons.Teamspeak]: "61709",
    [Icons.Telegram]: "61710",
    [Icons.Wire]: "61711",
};
