/*!
 *    Personal website of Sefa Eyeoglu
 *    Copyright (C) 2022  Sefa Eyeoglu <contact@scrumplex.net>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import OvenPlayer from "ovenplayer";
import ready from "./_utils";
import base64url from "base64url";

const streamUrlElem = document.getElementById("stream-url");

let refreshTimeout;
let player;

function isWebSocketURL(string) {
    let url;
    try {
        url = new URL(string);
    } catch {
        return false;  
    }

    return url.protocol === "ws:" || url.protocol === "wss:";
}


function playStream(streamUrl) {
    location.replace(`#${base64url.encode(streamUrl)}`);
    let sources = [];
    if (isWebSocketURL(streamUrl)) {
        sources.push(
            {
                label: "Source Bitrate",
                type: "webrtc",
                file: streamUrl
            },
            {
                label: "Lower Bitrate",
                type: "webrtc",
                file: streamUrl + "_good"
            }
        );
    }

    if (!player) {
        player = OvenPlayer.create("ovenplayer", {sources});
    } else {
        player.load({sources});
    }
}

streamUrlElem.addEventListener("keyup", () => {
    clearTimeout(refreshTimeout);
    refreshTimeout = setTimeout(() => {
        playStream(streamUrlElem.value);
    }, 1000);
});

ready().then(() => {
    const hash = document.location.hash.substring(1);
    const decoded = base64url.decode(hash);
    if (isWebSocketURL(decoded))
        streamUrlElem.value = decoded;
    playStream(decoded);
});
